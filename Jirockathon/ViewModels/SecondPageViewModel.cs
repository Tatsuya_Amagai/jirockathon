﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Prism.Navigation;
using System.Windows.Input;

namespace Jirockathon.ViewModels
{
	public class SecondPageViewModel : BindableBase
	{

		#region FIELDS

		private readonly INavigationService _navigationService;

		#endregion


		#region COMMANDS

		public ICommand GoBackCommand { get; }

		#endregion



		public SecondPageViewModel(INavigationService navigationService)
		{
			_navigationService = navigationService;
			GoBackCommand = new DelegateCommand(() =>
			{
				_navigationService.GoBackAsync();
			});
		}
	}
}
