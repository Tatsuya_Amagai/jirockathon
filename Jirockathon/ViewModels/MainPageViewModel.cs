﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Plugin.Beahat;
using Plugin.Beahat.Abstractions;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;

namespace Jirockathon.ViewModels
{
	public class MainPageViewModel : BindableBase, INavigationAware
	{
		/*
		private string _title;
		public string Title
		{
			get { return _title; }
			set { SetProperty(ref _title, value); }
		}
		*/

		private bool _isScanning = false;
		public bool IsScanning
		{
			get { return _isScanning; }
			set
			{
                SetProperty(ref _isScanning, value);
			}
		}
	

		#region FIELDS

		private readonly IBeahat _beahat;
        private readonly IPageDialogService _pageDialogService;
		private readonly INavigationService _navigationService;

		#endregion


		#region COMMANDS

		public ICommand StartScanCommand { get; }
		public ICommand StopScanCommand { get; }
		public ICommand NavigateSecondCommand { get; }

		#endregion

		public MainPageViewModel(IPageDialogService pageDialogService,
		                         INavigationService navigationService)
		{
			_beahat = Beahat.Current;
			_pageDialogService = pageDialogService;
			_navigationService = navigationService;

            StartScanCommand = new DelegateCommand(startScan);
            StopScanCommand = new DelegateCommand(stopScan);
			NavigateSecondCommand = new DelegateCommand(() =>
			{
				_navigationService.NavigateAsync("SecondPage");
			});
		}

		public void OnNavigatedFrom(NavigationParameters parameters)
		{

		}

		public void OnNavigatedTo(NavigationParameters parameters)
		{
			/*
			if (parameters.ContainsKey("title"))
				Title = (string)parameters["title"] + " and Prism";
			*/
		}

		public void OnNavigatingTo(NavigationParameters parameters)
		{

		}

		#region PRIVATE METHODS

		private void FoundJirorianCallback()
		{
			_pageDialogService.DisplayAlertAsync("ジロリアン発見！",
                                                 "仙台店がホームのジロリアンを発見しました！",
                                                 "OK");
			//IsScanning = true;
			/*
			Task.Delay(2000).Wait();
			JirorianIsNear = false;*/
		}

		private void startScan()
		{
			if (!_beahat.CanUseLocationForDetectBeacons())
            {
				_beahat.RequestToAllowUsingLocationForDetectBeacons();
                return;
            }

            _beahat.InitializeDetectedBeaconList();
			_beahat.AddObservableBeaconWithCallback(new Guid("FDA50693-A4E2-4FB1-AFCF-C6EB07647825"), -60, 2000, FoundJirorianCallback);
			_beahat.StartScan();

			IsScanning = _beahat.IsScanning;
		}

		private void stopScan()
		{
			_beahat.StopScan();
			IsScanning = _beahat.IsScanning;
		}

		#endregion
	}
}

